h1. Introduction

This script exists because my girlfriend wanted to be able to comment on her
Blogger/Blogspot posts directly from Google Reader, so here it is!  We'll
see about getting support for other blogs.

h2. Supported blogs

* Blogger/Blogspot
* WordPress

h1. Installing

Unfortunately, installing is a bit more of a hassle than it has to be mainly
because Greasemonkey thinks that the GitHub page that represents the script
is in fact the script itself.  Anyway, here goes:

# Install Greasemonkey (Firefox only)
# While on this page, click on the little Greasemonkey icon in your Firefox status bar to temporarily disable Greasemonkey
# If you want the release branch (you do) click "release" under "all branches" in the menu bar (on this GitHub page)
# Click on the google_reader_commenter.user.js link above this README
# Click on the "raw" link (next to "edit", "blame", and "history")
# Click on the Greasemonkey icon again to reenable Greasemonkey
# Refresh the page
# Install the script!

h1. Usage

Simply open your Google Reader as normal and note in the per-blog-post options (near the Share button) there will be a new Comment option.
Click on it to start your comment!  To close the dialog box, just click anywhere in the shrouded background (outside the comment box).

h1. Contributing

If you want to contribute support for other blog types or bug fixes, send me
a message and/or fork the repository, and you can send me a pull request later.

h1. License

Copyright (c) 2009 Max Aller <nanodeath@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the 'Software'), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
